---
title: Trung Nguyên Sáng Tạo 5
date: 2010-10-20 00:00:00
img: /medias/products/sang-tao-5.jpg
coverImg: /medias/products/trung-nguyen-sang-tao-5.jpg
masp: ST5-340
price: 93150
---

Lịch sử thế giới đã minh chứng, có những ý tưởng sáng tạo trước đây được coi là điên rồ, không tưởng thì nay đã thành hiện thực và làm thay đổi hoàn toàn thế giới và nó được khởi nguồn từ cà phê.

Một trong số các ý tưởng vĩ đại đó đã được truyền tải qua hình ảnh của bao bì Cà Phê Sáng Tạo – cà phê tuyệt ngon, chuyên cho não sáng tạo. Bao bì cà phê Sáng Tạo 5 là phát kiến về chiếc máy tính đầu tiên để tạo nên những siêu máy tính mạnh nhất của ngày nay. Được lựa chọn từ những hạt cà phê Culi Arabica tốt nhất thế giới, sản xuất trên công nghệ hàng đầu và bí quyết phối trộn không thể sao chép, cà phê Sáng tạo 5 dành cho những người thích tạo ra những kiểu uống theo gu của riêng mình.