---
title: Hello World
date: 2000-01-01 00:00:00
img: https://placeimg.com/640/360/animals
coverImg: https://placeimg.com/1366/768/animals
cover: true
author: 0ro
top: true
toc: false
mathjax: false
summary: Xin chào! Thế giới này thật bao la và chúng ta lại gặp nhau ở một nơi thật là lạ phải không nào. Bây giờ hãy chạm tùm lum để khám phá nhé, chúc bạn một này vui vẻ.
categories: me
tags: 
  - ăn
  - sáng tạo
---

Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)
