---
title: Trung Nguyên Sáng Tạo 2
date: 2010-10-20 00:00:00
img: /medias/products/sang-tao-2.jpg
coverImg: /medias/products/trung-nguyen-sang-tao-2.jpg
masp: ST2-340
price: 61800
---

Cà phê Sáng Tạo 2 là nơi khởi nguồn các sáng tạo mới. Với lòng đam mê sống chết với cà phê, Trung Nguyên tạo ra loại cà phê đặc biệt nhất thế giới, đem đến những ý tưởng đột phá giúp bạn thành công hơn trong cuộc sống. Sự kết hợp cà phê Robusta và Arabica xanh, sạch, thuần khiết từ vùng đất bazan nên tạo hương thơm quyến rũ, vị dịu nhẹ.

Bao bì của cà phê Sáng Tạo 2 là hình ảnh đầu máy xe lửa đầu tiên để tạo nên những những con tàu siêu tốc ngày nay, khẳng định về những ý tưởng và sự sáng tạo, có thể làm thay đổi, tạo ra những bước ngoặt trong lịch sử nhân loại.

Sáng tạo 2 có thể dùng để tặng những người yêu và am hiểu về cà phê.